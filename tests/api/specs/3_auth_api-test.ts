import { expect } from "chai";
import { RegisterController } from "../lib/controllers/register.controller";
import { AuthController } from "../lib/controllers/auth.controller";
import { UsersController } from "../lib/controllers/users.controller";
import { checkStatusCode } from "../../helpers/functionsForChecking.helper";

const register = new RegisterController();
const users = new UsersController();
const auth = new AuthController();

xdescribe("Token usage", () => {
    let accessToken: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCIsImN0eSI6IkpXVCJ9.eyJzdWIiOiJCU0EiLCJlbWFpbCI6IkJTQVdpbnRlckBnbWFpbC5jb20iLCJqdGkiOiJhMjVkYWFlMy0xZTFiLTQ4MzgtOTc5ZS1hMDgwMzQ3NTQ2ODMiLCJpYXQiOjE3MDY3MTk4MzAsImlkIjoiMjQyNCIsIm5iZiI6MTcwNjcxOTgyOSwiZXhwIjoxNzA2NzI3MDI5LCJpc3MiOiJUaHJlYWQuTkVUIFdlYkFQSSIsImF1ZCI6Imh0dHBzOi8vbG9jYWxob3N0OjQ0MzQ0In0.3s38obLHFtkS8wZMf_--fPkxVsHft5_XPz6J9skyw2Q";

    before(`Login and get the token`, async () => {
        let response = await auth.login("BSAWinter@gmail.com", "BSA1!");
 
        accessToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCIsImN0eSI6IkpXVCJ9.eyJzdWIiOiJCU0EiLCJlbWFpbCI6IkJTQVdpbnRlckBnbWFpbC5jb20iLCJqdGkiOiJhMjVkYWFlMy0xZTFiLTQ4MzgtOTc5ZS1hMDgwMzQ3NTQ2ODMiLCJpYXQiOjE3MDY3MTk4MzAsImlkIjoiMjQyNCIsIm5iZiI6MTcwNjcxOTgyOSwiZXhwIjoxNzA2NzI3MDI5LCJpc3MiOiJUaHJlYWQuTkVUIFdlYkFQSSIsImF1ZCI6Imh0dHBzOi8vbG9jYWxob3N0OjQ0MzQ0In0.3s38obLHFtkS8wZMf_--fPkxVsHft5_XPz6J9skyw2Q";
        // console.log(accessToken);
    });

    it(`Usage is here`, async () => {
        let userData: object = {
            id: 2424,
            avatar: "string",
            email: "BSAWinter@gmail.com",
            userName: "BSA",
        };

        let response = await users.updateUser(userData, "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCIsImN0eSI6IkpXVCJ9.eyJzdWIiOiJCU0EiLCJlbWFpbCI6IkJTQVdpbnRlckBnbWFpbC5jb20iLCJqdGkiOiJhMjVkYWFlMy0xZTFiLTQ4MzgtOTc5ZS1hMDgwMzQ3NTQ2ODMiLCJpYXQiOjE3MDY3MTk4MzAsImlkIjoiMjQyNCIsIm5iZiI6MTcwNjcxOTgyOSwiZXhwIjoxNzA2NzI3MDI5LCJpc3MiOiJUaHJlYWQuTkVUIFdlYkFQSSIsImF1ZCI6Imh0dHBzOi8vbG9jYWxob3N0OjQ0MzQ0In0.3s38obLHFtkS8wZMf_--fPkxVsHft5_XPz6J9skyw2Q");
        checkStatusCode(response, 204);
    });
});
