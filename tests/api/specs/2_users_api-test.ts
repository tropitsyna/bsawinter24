import { expect } from "chai";
import { RegisterController } from "../lib/controllers/register.controller";
import { AuthController } from "../lib/controllers/auth.controller";
import { UsersController } from "../lib/controllers/users.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";

const register = new RegisterController();
const auth = new AuthController();
const users = new UsersController();
const schemas = require('./data/schemas_testData.json');
const chai = require('chai');
chai.use(require('chai-json-schema'));

xdescribe(`Users controller`, () => {
    let userId: number;
 
    it(`should return 200 status code and all users when getting the user collection`, async () => {
        let response = await users.getAllUsers();

        // console.log("All Users:");
        // console.log(response.body);

        checkStatusCode(response, 200);
        checkResponseTime(response,1000);
        expect(response.body.length, `Response body should have more than 1 item`).to.be.greaterThan(1); 
        expect(response.body).to.be.jsonSchema(schemas.schema_allUsers); 
        
        userId = response.body[1].id;
    });

    it(`should return 200 when getting user details with valid id`, async () => {
        let validUserId = 2424

        let response = await users.getUserById(validUserId);
 
        checkStatusCode(response, 200);
        checkResponseTime(response,1000);
    });

    it(`should return 404 error when getting user details with invalid id`, async () => {
        let invalidUserId = 1690555

        let response = await users.getUserById(invalidUserId);
 
        checkStatusCode(response, 404);
        checkResponseTime(response,1000);
    });

    it(`should return 400 error when getting user details with invalid id type`, async () => {
        let invalidUserId = '885906069676656448596006'

        let response = await users.getUserById(invalidUserId);

        checkStatusCode(response, 400);
        checkResponseTime(response,1000);
    });

    it(`should return user details when getting user details with valid id`, async () => {
        let response = await users.getAllUsers();
        let firstUserId: number = response.body[0].id;
        
        response = await users.getUserById(firstUserId);
        
        checkStatusCode(response, 200);
        checkResponseTime(response,1000);

        // console.log(response.body);
    });

    it(`Delete user`, async () => {
        let userData: object = {
            id: 2424,
        };

        let response = await users.DeleteUserById("2424", userData, "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCIsImN0eSI6IkpXVCJ9.eyJzdWIiOiJCU0EiLCJlbWFpbCI6IkJTQVdpbnRlckBnbWFpbC5jb20iLCJqdGkiOiJhMjVkYWFlMy0xZTFiLTQ4MzgtOTc5ZS1hMDgwMzQ3NTQ2ODMiLCJpYXQiOjE3MDY3MTk4MzAsImlkIjoiMjQyNCIsIm5iZiI6MTcwNjcxOTgyOSwiZXhwIjoxNzA2NzI3MDI5LCJpc3MiOiJUaHJlYWQuTkVUIFdlYkFQSSIsImF1ZCI6Imh0dHBzOi8vbG9jYWxob3N0OjQ0MzQ0In0.3s38obLHFtkS8wZMf_--fPkxVsHft5_XPz6J9skyw2Q");
        checkStatusCode(response, 204);
    });
});

