import {
    checkResponseTime,
    checkStatusCode, 
} from '../../helpers/functionsForChecking.helper';
import { AuthController } from '../lib/controllers/auth.controller';
const auth = new AuthController();

describe('Use test data set for login', () => {
    let invalidCredentialsDataSet = [
        { email: 'BSAWinter@gmail.com', password: '' },
        { email: 'BSAWinter@gmail.com', password: '      ' },
        { email: 'BSAWinter@gmail.com', password: 'BSA! ' },
        { email: 'BSAWinter@gmail.com', password: 'DSA1' },
        { email: 'BSAWinter@gmail.com', password: 'BS1!' },
        { email: 'BSAWinter@gmail.com', password: 'BSAWinter@gmail.com' },
    ];

    invalidCredentialsDataSet.forEach((credentials) => {
        it(`should not login using invalid credentials : '${credentials.email}' + '${credentials.password}'`, async () => {
            let response = await auth.login(credentials.email, credentials.password);

            checkStatusCode(response, 401); 
            checkResponseTime(response, 3000);
        });
    });
});

