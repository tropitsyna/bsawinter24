import { expect } from "chai";
import { RegisterController } from "../lib/controllers/register.controller";
import { UsersController } from "../lib/controllers/users.controller";
import { AuthController } from "../lib/controllers/auth.controller";
import { checkStatusCode } from "../../helpers/functionsForChecking.helper";

const register = new RegisterController();
const users = new UsersController();
const auth = new AuthController();

xdescribe("Update user | without hooks", () => {
    let accessToken: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCIsImN0eSI6IkpXVCJ9.eyJzdWIiOiJCU0EiLCJlbWFpbCI6IkJTQVdpbnRlckBnbWFpbC5jb20iLCJqdGkiOiJhMjVkYWFlMy0xZTFiLTQ4MzgtOTc5ZS1hMDgwMzQ3NTQ2ODMiLCJpYXQiOjE3MDY3MTk4MzAsImlkIjoiMjQyNCIsIm5iZiI6MTcwNjcxOTgyOSwiZXhwIjoxNzA2NzI3MDI5LCJpc3MiOiJUaHJlYWQuTkVUIFdlYkFQSSIsImF1ZCI6Imh0dHBzOi8vbG9jYWxob3N0OjQ0MzQ0In0.3s38obLHFtkS8wZMf_--fPkxVsHft5_XPz6J9skyw2Q";
    let userDataBeforeUpdate, userDataToUpdate;

    it(`Login and get the token`, async () => {
        let response = await auth.login("BSAWinter@gmail.com", "BSA1!");
        checkStatusCode(response, 200);
        accessToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCIsImN0eSI6IkpXVCJ9.eyJzdWIiOiJCU0EiLCJlbWFpbCI6IkJTQVdpbnRlckBnbWFpbC5jb20iLCJqdGkiOiJhMjVkYWFlMy0xZTFiLTQ4MzgtOTc5ZS1hMDgwMzQ3NTQ2ODMiLCJpYXQiOjE3MDY3MTk4MzAsImlkIjoiMjQyNCIsIm5iZiI6MTcwNjcxOTgyOSwiZXhwIjoxNzA2NzI3MDI5LCJpc3MiOiJUaHJlYWQuTkVUIFdlYkFQSSIsImF1ZCI6Imh0dHBzOi8vbG9jYWxob3N0OjQ0MzQ0In0.3s38obLHFtkS8wZMf_--fPkxVsHft5_XPz6J9skyw2Q";
    });

    it(`should return correct details of the currect user`, async () => {
        let response = await users.getCurrentUser("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCIsImN0eSI6IkpXVCJ9.eyJzdWIiOiJCU0EiLCJlbWFpbCI6IkJTQVdpbnRlckBnbWFpbC5jb20iLCJqdGkiOiJhMjVkYWFlMy0xZTFiLTQ4MzgtOTc5ZS1hMDgwMzQ3NTQ2ODMiLCJpYXQiOjE3MDY3MTk4MzAsImlkIjoiMjQyNCIsIm5iZiI6MTcwNjcxOTgyOSwiZXhwIjoxNzA2NzI3MDI5LCJpc3MiOiJUaHJlYWQuTkVUIFdlYkFQSSIsImF1ZCI6Imh0dHBzOi8vbG9jYWxob3N0OjQ0MzQ0In0.3s38obLHFtkS8wZMf_--fPkxVsHft5_XPz6J9skyw2Q");
        checkStatusCode(response, 200);
        userDataBeforeUpdate = response.body;
    });

    it(`should update username using valid data`, async () => {
        // replace the last 3 characters of actual username with random characters.
        // Another data should be without changes
        function replaceLastThreeWithRandom(str: string): string {
            return str.slice(0, -3) + Math.random().toString(36).substring(2, 5);
        }

        userDataToUpdate = {
            id: userDataBeforeUpdate.id,
            avatar: userDataBeforeUpdate.avatar,
            email: userDataBeforeUpdate.email,
            userName: replaceLastThreeWithRandom(userDataBeforeUpdate.userName),
        };

        let response = await users.updateUser(userDataToUpdate, "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCIsImN0eSI6IkpXVCJ9.eyJzdWIiOiJCU0EiLCJlbWFpbCI6IkJTQVdpbnRlckBnbWFpbC5jb20iLCJqdGkiOiJhMjVkYWFlMy0xZTFiLTQ4MzgtOTc5ZS1hMDgwMzQ3NTQ2ODMiLCJpYXQiOjE3MDY3MTk4MzAsImlkIjoiMjQyNCIsIm5iZiI6MTcwNjcxOTgyOSwiZXhwIjoxNzA2NzI3MDI5LCJpc3MiOiJUaHJlYWQuTkVUIFdlYkFQSSIsImF1ZCI6Imh0dHBzOi8vbG9jYWxob3N0OjQ0MzQ0In0.3s38obLHFtkS8wZMf_--fPkxVsHft5_XPz6J9skyw2Q");
        checkStatusCode(response, 204);
    });

    it(`should return correct user details by id after updating`, async () => {
        let response = await users.getUserById(userDataBeforeUpdate.id);
        checkStatusCode(response, 200);
        expect(response.body).to.be.deep.equal(userDataToUpdate, "User details isn't correct");
    });
});
