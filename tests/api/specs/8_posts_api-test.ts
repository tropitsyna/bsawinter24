import { expect } from "chai";
import { RegisterController } from "../lib/controllers/register.controller";
import { AuthController } from "../lib/controllers/auth.controller";
import { UsersController } from "../lib/controllers/users.controller";
import { PostsController } from "../lib/controllers/posts.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";

const register = new RegisterController();
const auth = new AuthController();
const users = new UsersController();
const posts = new PostsController();
const schemas = require('./data/schemas_testData.json');
const chai = require('chai');
chai.use(require('chai-json-schema'));

xdescribe(`Posts controller`, () => {
    let userId: number;
 
    it(`should return 200 status code and all posts when getting the posts collection`, async () => {
        let response = await posts.getAllexistingPosts();

        // console.log("All Posts:");
        // console.log(response.body);

        checkStatusCode(response, 200);
        checkResponseTime(response,1000);
        expect(response.body.length, `Response body should have more than 1 item`).to.be.greaterThan(1); 
        expect(response.body).to.be.jsonSchema(schemas.schema_allUsers); 
        
    });

});
