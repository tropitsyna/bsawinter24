import { checkResponseTime, checkStatusCode } from "../../helpers/functionsForChecking.helper";
import { AuthController } from "../lib/controllers/auth.controller";
const auth = new AuthController();

xdescribe("Without test data set for login - bad example", () => {
    it(`should not login using invalid credentials email: 'BSAWinter@gmail.com', password: ''`, async () => {
        let response = await auth.login("BSAWinter@gmail.com", "");

        checkStatusCode(response, 401);
        checkResponseTime(response, 3000);
    });

    it(`should not login using invalid credentials email: 'BSAWinter@gmail.com', password: '      '`, async () => {
        let response = await auth.login("BSAWinter@gmail.com", "      ");

        checkStatusCode(response, 401);
        checkResponseTime(response, 3000);
    });

    it(`should not login using invalid credentials email: 'BSAWinter@gmail.com', password: 'DSA1'`, async () => {
        let response = await auth.login("BSAWinter@gmail.com", "DSA1");

        checkStatusCode(response, 401);
        checkResponseTime(response, 3000);
    });

    it(`should not login using invalid credentials email: 'BSAWinter@gmail.com', password: 'BS1!'`, async () => {
        let response = await auth.login("BSAWinter@gmail.com", "BS1!");

        checkStatusCode(response, 401);
        checkResponseTime(response, 3000);
    });

    it(`should not login using invalid credentials email: 'BSAWinter@gmail.com', password: 'BSAWinter@gmail.com'`, async () => {
        let response = await auth.login("BSAWinter@gmail.com", "BSAWinter@gmail.com");

        checkStatusCode(response, 401);
        checkResponseTime(response, 3000);
    });
});
