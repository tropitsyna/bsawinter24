import { ApiRequest } from "../request";

const baseUrl = "http://tasque.lol/";

export class UsersController {
    async getAllUsers() {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`api/Users`)
            .send();
        return response;
    }

    async getUserById(id) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`api/Users/${id}`)
            .send();
        return response;
    }

    async getCurrentUser(accessToken: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCIsImN0eSI6IkpXVCJ9.eyJzdWIiOiJCU0EiLCJlbWFpbCI6IkJTQVdpbnRlckBnbWFpbC5jb20iLCJqdGkiOiJhMjVkYWFlMy0xZTFiLTQ4MzgtOTc5ZS1hMDgwMzQ3NTQ2ODMiLCJpYXQiOjE3MDY3MTk4MzAsImlkIjoiMjQyNCIsIm5iZiI6MTcwNjcxOTgyOSwiZXhwIjoxNzA2NzI3MDI5LCJpc3MiOiJUaHJlYWQuTkVUIFdlYkFQSSIsImF1ZCI6Imh0dHBzOi8vbG9jYWxob3N0OjQ0MzQ0In0.3s38obLHFtkS8wZMf_--fPkxVsHft5_XPz6J9skyw2Q") {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`api/Users/fromToken`)
            .bearerToken(accessToken)
            .send();
        return response;
    }
   
    async updateUser(userData: object, accessToken: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCIsImN0eSI6IkpXVCJ9.eyJzdWIiOiJCU0EiLCJlbWFpbCI6IkJTQVdpbnRlckBnbWFpbC5jb20iLCJqdGkiOiJhMjVkYWFlMy0xZTFiLTQ4MzgtOTc5ZS1hMDgwMzQ3NTQ2ODMiLCJpYXQiOjE3MDY3MTk4MzAsImlkIjoiMjQyNCIsIm5iZiI6MTcwNjcxOTgyOSwiZXhwIjoxNzA2NzI3MDI5LCJpc3MiOiJUaHJlYWQuTkVUIFdlYkFQSSIsImF1ZCI6Imh0dHBzOi8vbG9jYWxob3N0OjQ0MzQ0In0.3s38obLHFtkS8wZMf_--fPkxVsHft5_XPz6J9skyw2Q") {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("PUT")
            .url(`api/Users`)
            .body(userData)
            .bearerToken(accessToken)
            .send();
        return response;
    }

 
    async DeleteUserById(id, userData: object, accessToken: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCIsImN0eSI6IkpXVCJ9.eyJzdWIiOiJCU0EiLCJlbWFpbCI6IkJTQVdpbnRlckBnbWFpbC5jb20iLCJqdGkiOiJhMjVkYWFlMy0xZTFiLTQ4MzgtOTc5ZS1hMDgwMzQ3NTQ2ODMiLCJpYXQiOjE3MDY3MTk4MzAsImlkIjoiMjQyNCIsIm5iZiI6MTcwNjcxOTgyOSwiZXhwIjoxNzA2NzI3MDI5LCJpc3MiOiJUaHJlYWQuTkVUIFdlYkFQSSIsImF1ZCI6Imh0dHBzOi8vbG9jYWxob3N0OjQ0MzQ0In0.3s38obLHFtkS8wZMf_--fPkxVsHft5_XPz6J9skyw2Q") {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("DELETE")
            .url(`api/Users/${id}`)
            .bearerToken(accessToken)
            .send();
        return response;
    }

}