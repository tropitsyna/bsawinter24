import { ApiRequest } from "../request";

export class PostsController {
    async getAllexistingPosts() {
        const response = await new ApiRequest()
            .prefixUrl("http://tasque.lol/")
            .method("GET")
            .url(`api/Users`)
            .send();
        return response;

    }

    async posts(authorId: 0, previewImageValue: string, bodyValue: string) 
    {
        const response = await new ApiRequest()
            .prefixUrl("http://tasque.lol/")
            .method("POST")
            .url(`api/Register`)
            .body({
                authorId: 0,
                previewImage: previewImageValue,
                body: bodyValue,
            })
            .send();
        return response;
    }

    async postLike(entityId: 0, isLikeValue: true, userIdValue: 0) 
    {
        const response = await new ApiRequest()
            .prefixUrl("http://tasque.lol/")
            .method("POST")
            .url(`api/Register`)
            .body({
                entityId: 0,
                isLike: isLikeValue,
                userId: userIdValue,
            })
            .send();
        return response;
    }
}