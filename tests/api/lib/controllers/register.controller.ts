import { ApiRequest } from "../request";

export class RegisterController {
    async login(avatarValue: string, emailValue: string, UserNameValue: string, passwordValue: string) {
        const response = await new ApiRequest()
            .prefixUrl("http://tasque.lol/")
            .method("POST")
            .url(`api/Register`)
            .body({
                id: 0,
                avatar: avatarValue,
                email: emailValue,
                userName: UserNameValue,
                password: passwordValue,
            })
            .send();
        return response;
    }
}